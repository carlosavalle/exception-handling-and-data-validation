package avalle;


import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        float  num1,num2 = 0;
        Boolean loop = true;
        float result = 0;
        while (loop) {
            try {

                Scanner input = new Scanner( System.in );

                System.out.print("Enter the first number: \n");
                //store the first number
                num1 = input.nextFloat();

                System.out.print("Enter the second number: \n");
                //store the second number
                num2 = input.nextFloat();

                //will validate the denominator != 0
                if  (Validator.validateDenominator(num2)){
                    // will store the divison
                    result = doDivision(num1, num2);
                    loop =false;
                }else {
                    System.out.printf("Enter a valid denominator\n");
                }


            }catch (InputMismatchException ime) {
                System.out.println("Please, enter a valid number \n");
            } catch (ArithmeticException e) {
                System.out.println("It is not possible to division by zero \n");
            } finally {
                if (loop){
                    System.out.println("Please, try it again \n");
                }else{
                    System.out.println("The result is: " + result);
                }

            }
        }
    }

    public static float doDivision(float num1, float num2) throws ArithmeticException{
        float division ;
        // will check if denominator !=0
        if (num2 !=0) {
            division= (float) num1 / num2;
        }else{
            //if not will throw exception
            throw (new ArithmeticException());
        }
          return division;
    }
}
